*** Settings ***
#Resource          Core/CommonResources/BaseTest.robot
Resource           PDF-WORD-EXCEL/Resources/StepDefinitions/BDDKeywords.robot


*** Variables ***
${FilePath_PDF}              ${CURDIR}\\Files\\pdf1.pdf
${FilePath_EXCEL}            ${CURDIR}\\Files\\excel2.xls
${Excel_Sheetname}           Retailinsrerate2_MKEY_TEST_2022


*** Test Cases ***
Poc for PDF, Word and Excel Validations
    #Prerequisite - Already captured the Word constansts.
    Given I convert the PDF file into text document                         ${FilePath_PDF}
      AND capture the Unique key - Customer number from the PDF data        ${PDF_Data_detail}
    When I open the Excel and read Row and Column count                     ${FilePath_EXCEL}     ${Excel_Sheetname}
      AND capture the actual excel row data                                 ${CONSTANT_Customer_Number}       ${PDFdata_CustomerNumber}
    Then validate the Excel and PDF data                                    ${PDFdata_CustomerNumber}   ${Excel_Col_No}     ${Excel_Row_No}















#    ${my_text} =   doc2text.Document.process     ${CURDIR}\\Files\\word2.pdf
#    log to console    ${my_text}
#     Copy File              ${CURDIR}\\Files\\word3.docx         ${CURDIR}\\Files\\word3.txt
#     ${detail_1}     Get File              ${CURDIR}\\Files\\word3.txt
#    Log To Console        ${detail_1}















*** Keywords ***


