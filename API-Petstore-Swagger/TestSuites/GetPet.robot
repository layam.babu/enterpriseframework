*** Settings ***
Documentation     API Testing in Robot Framework
Resource          Core/CommonResources/BaseTest.robot
Resource          API-Petstore-Swagger/Resources/StepDefinitions/BDDKeywords.robot
Test Setup        Base API Test Setup        mysession
#Test Teardown     Base API Test Tear Down

*** Variables ***

*** Test Cases ***
Do a GET Request and validate the response code and response body
    [documentation]  This test case verifies that the response code of the GET Request should be 200,
    ...  the response body contains the 'name' key with value as 'AnimalName',
    [Tags]    SmokeTesting

    Given I run the GetService name called - "Get Pet"
    When I validate the "Get Pet" Response code
    Then I validate the "Get Pet" Response body




#Do a POST Request and validate the response code, response body, and response headers
#    [documentation]  This test case verifies that the response code of the POST Request should be 201,
#    ...  the response body contains the 'id' key with value '101',
#    ...  and the response header 'Content-Type' has the value 'application/json; charset=utf-8'.
#    [tags]  Regression
#    Create Session  mysession  https://jsonplaceholder.typicode.com  verify=true
#    &{body}=  Create Dictionary  title=foo  body=bar  userId=9000
#    &{header}=  Create Dictionary  Cache-Control=no-cache
#    ${response}=  POST On Session  mysession  /posts  data=${body}  headers=${header}
#    Status Should Be  201  ${response}  #Check Status as 201
#
#    #Check id as 101 from Response Body
#    ${id}=  Get Value From Json  ${response.json()}  id
#    ${idFromList}=  Get From List   ${id}  0
#    ${idFromListAsString}=  Convert To String  ${idFromList}
#    Should be equal As Strings  ${idFromListAsString}  101
#
#    #Check the value of the header Content-Type
#    ${getHeaderValue}=  Get From Dictionary  ${response.headers}  Content-Type
#    Should be equal  ${getHeaderValue}  application/json; charset=utf-8
#
#*** Keywords ***
