*** Settings ***
Resource   Core/CommonResources/Utilities.robot


*** Variables ***
${TEXT_ShopPage_StuffedFrog_Price}                  xpath=//h4[.="Stuffed Frog"]/parent::div//span
${LINK_ShopPage_StuffedFrog_Buy}                    xpath=//h4[.="Stuffed Frog"]/parent::div//a
${TEXT_ShopPage_ValentineBear_Price}                xpath=//h4[.="Valentine Bear"]/parent::div//span
${LINK_ShopPage_ValentineBear_Buy}                  xpath=//h4[.="Valentine Bear"]/parent::div//a
${TEXT_ShopPage_FluffyBunny_Price}                  xpath=//h4[.="Fluffy Bunny"]/parent::div//span
${LINK_ShopPage_FluffyBunny_Buy}                    xpath=//h4[.="Fluffy Bunny"]/parent::div//a
${TEXT_ShopPage_TeddyBear_Price}                    xpath=//h4[.="Teddy Bear"]/parent::div//span
${LINK_ShopPage_TeddyBear_Buy}                      xpath=//h4[.="Teddy Bear"]/parent::div//a
${TEXT_ShopPage_HandmadeDoll_Price}                 xpath=//h4[.="Handmade Doll"]/parent::div//span
${LINK_ShopPage_HandmadeDoll_Buy}                   xpath=//h4[.="Handmade Doll"]/parent::div//a
${TEXT_ShopPage_SmileyBear_Price}                   xpath=//h4[.="Smiley Bear"]/parent::div//span
${LINK_ShopPage_SmileyBear_Buy}                     xpath=//h4[.="Smiley Bear"]/parent::div//a
${TEXT_ShopPage_FunnyCow_Price}                     xpath=//h4[.="Funny Cow"]/parent::div//span
${LINK_ShopPage_FunnyCow_Buy}                       xpath=//h4[.="Funny Cow"]/parent::div//a
${TEXT_ShopPage_SmileyFace_Price}                   xpath=//h4[.="Smiley Face"]/parent::div//span
${LINK_ShopPage_SmileyFace_Buy}                     xpath=//h4[.="Smiley Face"]/parent::div//a

*** Keywords ***
shopPage.Capture the Price and Add to cart
    [Arguments]     ${BuyProductTimes}     ${BuyProductPrice}    ${BuyProductToCart}   ${itemName}
    ${ActualPrice_Product}    get Text   ${BuyProductPrice}

    run keyword if   '${itemName}'=='StuffedFrog'       run keyword and continue on failure    Should Be Equal As Strings    ${ActualPrice_Product}    ${ExpectedPrice_StuffedFrog}
    ...  ELSE IF   '${itemName}'=='TeddyBear'           run keyword and continue on failure    Should Be Equal As Strings    ${ActualPrice_Product}    ${ExpectedPrice_TeddyBear}
    ...  ELSE IF   '${itemName}'=='HandmadeDoll'        run keyword and continue on failure    Should Be Equal As Strings    ${ActualPrice_Product}    ${ExpectedPrice_HandmadeDoll}
    ...  ELSE IF   '${itemName}'=='FluffyBunny'         run keyword and continue on failure    Should Be Equal As Strings    ${ActualPrice_Product}    ${ExpectedPrice_FluffyBunny}
    ...  ELSE IF   '${itemName}'=='SmileyBear'          run keyword and continue on failure    Should Be Equal As Strings    ${ActualPrice_Product}    ${ExpectedPrice_SmileyBear}
    ...  ELSE IF   '${itemName}'=='FunnyCow'            run keyword and continue on failure    Should Be Equal As Strings    ${ActualPrice_Product}    ${ExpectedPrice_FunnyCow}
    ...  ELSE IF   '${itemName}'=='ValentineBear'       run keyword and continue on failure    Should Be Equal As Strings    ${ActualPrice_Product}    ${ExpectedPrice_ValentineBear}
    ...  ELSE IF     '${itemName}'=='SmileyFace'          run keyword and continue on failure    Should Be Equal As Strings    ${ActualPrice_Product}    ${ExpectedPrice_SmileyFace}

    FOR  ${item}  IN RANGE   0   ${BuyProductTimes}
        set selenium speed  0.5s
        Click element    ${BuyProductToCart}
        set selenium speed  0s
    END


