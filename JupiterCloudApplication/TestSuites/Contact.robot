*** Settings ***
Documentation     PoC Script to Check the Framework Features
Resource          Core/CommonResources/BaseTest.robot
Resource          JupiterCloudApplication/Resources/StepDefinitions/BDDKeywords.robot
Force Tags        Portfolio:CRM    Application:JupiterCloud

Test Setup        Base Web Test Setup   ${ENV_BROWSERTYPE}
Test Teardown     Base Web Test Tear Down

*** Variables ***


*** Test Cases ***
Contact.TestCase1
    [Tags]  contactTC
    Given I log into JupiterCloud applicaion
     And From the home page go to contact page
    When Click submit button
     And SeleniumLibrary.Capture Page Screenshot
     And Verify error messages
     And Populate mandatory fields
    Then Validate errors are gone
     And SeleniumLibrary.Capture Page Screenshot

Contact.TestCase2
    [Tags]  contactTC
        Given I log into JupiterCloud applicaion
         And From the home page go to contact page
        When Populate mandatory fields
         And Click submit button
         And SeleniumLibrary.Capture Page Screenshot
        Then Validate successful submission message        #Note: Run this test 5 times to ensure 100% pass rate


