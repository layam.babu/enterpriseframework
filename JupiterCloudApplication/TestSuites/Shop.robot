*** Settings ***
Documentation     PoC Script to Check the Framework Features
Resource          Core/CommonResources/BaseTest.robot
Resource          JupiterCloudApplication/Resources/StepDefinitions/BDDKeywords.robot
Force Tags        Portfolio:CRM    Application:JupiterCloud

Test Setup        Base Web Test Setup   ${ENV_BROWSERTYPE}
Test Teardown     Base Web Test Tear Down
Test Template     Shop.TestCase1

*** Test Cases ***
########${StuffedFrog}  ${FluffyBunny}  ${ValentineBear}  ${TeddyBear}  ${HandmadeDoll}  ${SmileyBear}  ${FunnyCow}  ${SmileyFace}
TC1     1   0    0   0   0   0  0  1
TC2     1   1    1   0   0   0  0  1
TC3     1   0    0   1   1   0  0  1
TC4     1   0    0   0   0   1  1  1


*** Keywords ***
Shop.TestCase1
    [Tags]  shopTC
    [Arguments]    ${StuffedFrog}  ${FluffyBunny}  ${ValentineBear}  ${TeddyBear}  ${HandmadeDoll}  ${SmileyBear}  ${FunnyCow}  ${SmileyFace}
    Given I log into JupiterCloud applicaion
     And From the home page go to Shop page
    When Buy products and validate the expected price in Shopping page  StuffedFrog=2   FluffyBunny=0   ValentineBear=0    TeddyBear=0     HandmadeDoll=0     SmileyBear=0    FunnyCow=0     SmileyFace=0
     And SeleniumLibrary.Capture Page Screenshot
    Then Verify the subtotal for each product is correct in Cart Page
     And SeleniumLibrary.Capture Page Screenshot
     And Verify the price for each product is correct in Cart Page
