*** Settings ***
    [Documentation]    This section needs to include all the resources robot files to ensure there is an easy connection between Keywords in this page with others in the Framework
Resource   FrameworkModel/Resources/PageObjects/Page1.robot
Resource   Core/CommonResources/Utilities.robot

*** Variables ***
    [Documentation]   This section is not mandatory as the Variables are defined in the POM file
    ...     any variable that is not on POM but want to send Test Data. This space can be used


*** Keywords ***
Keyword1
    page1.actual test step1
    page2.actual test step2

Keyword2
    page1.actual test step2

Keyword3
    page1.actual test step1

Keyword4
    page1.actual test step2

Keyword5
    page1.actual test step1

Keyword6
    page1.actual test step1
